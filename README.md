Free source code for [Learn Kubernetes & Docker book](https://leanpub.com/k8s) demos and do-it-yourself

# Get the book

Buy the book here:

* print book on [Lulu](http://www.lulu.com/content/livre-%c3%a0-couverture-souple/learn-kubernetes-docker---net-core-java-nodejs-php-or-python/26217672).
* ebook on [Amazon](http://mybook.to/k8s) and [Leanpub](https://leanpub.com/k8s).

# Get the source code

Just `git clone` that repository. If that sounds obscure to you, click the "Downloads" link at the left of this window.

# Book TOC

1. Why Docker?
2. Why Kubernetes?
3. Get Docker up and running
4. Basic concepts
5. Use Docker images
6. Create Docker images
7. Publish Docker images
8. Forget SDK installs
9. Docker with common development profiles
10. Kubernetes cluster
11. Tooling
12. Running pods
13. Exposing services
14. Volumes
15. Configuration
16. Updating and scaling
17. Sharing a cluster
18. Helm
